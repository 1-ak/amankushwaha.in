var imageAddr = "https://amankushwaha.in/random.png";
var downloadSize = 3151999;

var startTime, endTime;
var download = new Image();
download.onload = function () {
    endTime = (new Date()).getTime();
    showResults();
}

startTime = (new Date()).getTime();
download.src = imageAddr;

function showResults() {
    var duration = (endTime - startTime) / 1000;
    var bitsLoaded = downloadSize;
    var speedBps = (bitsLoaded / duration).toFixed(2);
    var speedKbps = (speedBps / 1024).toFixed(2);
    var speedMbps = (speedKbps / 1024).toFixed(2);

document.getElementById("myText").innerHTML = speedMbps;
